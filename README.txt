CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Google & Site Configuration
  * API functions
  * API example


INTRODUCTION
------------
This module allows you to authenticate with Google, using a service
account and OAuth 2.0. You need to create a service account using
Google Developers Console or a similar tool. Service accounts use
hidden, prepared authentication files instead of passwords supplied
at access time. This makes them suitable for cron tasks and other
site-controlled access to Google services.

You need a generated key-pair file, with extension .p12, and the email 
address created to authenticate the service account. Other fields 
returned by the service-account definition process are not used by this
module. To access items such as calendars, the email address used in
this module must have permission to access the information as necessary.
The key-pair file should be stored in an area NOT accessible to the web,
such as the configured private file directory.

This module is only used to configure and validate service account
parameters and to provide a method to return a new Google_Client to use
for API access.

It was inspired by the gauth module, which allowed connection using email
and user-entered password. Details on service-account connection setup
and use were found in the 1.1.1 version of the google-api-php-client
library.

At this time, the most current version of the PHP client library may be 
found at https://github.com/google/google-api-php-client. That is the
only version available, and is subject to revision. The library should
be downloaded into the libraries directory as google-api-php-client.
For production, the examples and tests directories may be removed. All
other files and directories should remain.

This module is very lightweight and is not associated with a specific version
of the Google library. This avoids tying the account connection to a specific
set of other Google functionality.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/vgriffin/2390251
   
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2390251


REQUIREMENTS
------------
  * Libraries API - This modules depends on libraries apis module. You can 
    download from http://drupal.org/project/libraries
    
  * Google client library - You need to download the google api php client
    library from https://github.com/google/google-api-php-client. The status
    report shows the loaded version.


INSTALLATION
------------

  * Copy this module directory to your sites/all/modules or
    sites/SITENAME/modules directory.

  * Download the latest release of google php client library and
    extract it in libraries folder of the site, usually located at
    sites/all/libraries, the path into which code should be extracted is 
    'sites/all/libraries/google-api-php-client'. The only sub-directory
    used in production is 'src', although the 'examples' directory may be
    useful if you are developing other modules that use Google Apps.
    
    Note that this code was developed and tested with the 1.1.1 release of
    google-api-php-client. It has also been tested and used with the 1.1.5
    release of that module. (The version is the value of LIBVER in the file
    src/Google/Client.php.)

  * Enable the module and define settings at 
    admin/config/services/gauth_service_account.


GOOGLE & SITE CONFIGURATION
---------------------------

  * Visit https://console.developers.google.com. If you have a Google Apps
    account you wish to use for authentication, use an administrator login
    to login to the developers console. Note that this module uses OAuth2
    and a service account. For this access, you do not need API keys.
   
  * Create a new service-account project with appropriate details,
    if you don't have a project created.
   
  * Save the ...p12 file and note the client-id and email address 
    associated with the service account. At this time, the public key
    fingerprint value is not used by the PHP library.
   
  * Ensure that the APIs you wish to use are enabled for the project. If
    you miss APIs, or want to add APIs, you may do so later.
   
  * If there are Google calendars or other content you wish to access,
    enable access by the email address returned when you created the ...p12
    file. (There may be other ways to enable account access. This method
    has been demonstrated to work as of 09/03/2015.)
   
  * Place the ...p12 file in a location on your web server which may be
    used by Drupal code but is not visible from the web. The private file
    directory may be used for this.
   
  * Enter the key-pair (...p12) file address, and email address in the 
    GAuth Service Account configuration screen accessible at
    admin/config/services/gauth_account. Save and test the connection.
    Note that this test does not verify that API or data access has been
    enabled.


API FUNCTIONS
-------------

gauth_svc_acct_client_get($appname, $scopes = array(), $sub_email, 
    $force_token = FALSE)

    This function will return a Google_Client object for the configured service
    account, associated with the specified scope(s). At least one scope must be
    specified. If the service account has been granted domain access, an email
    address in the domain may be used for access.

    The $force_token flag may be used to force an immediate refresh of any
    existing token. If it is set, the token credentials are refreshed and
    tested even if the token has not expired. (This is how the configuration
    test works.)

    If the credentials do not work, an Exception is thrown. Therefore, it is
    essential to use try/catch blocks when this function is called.

    To get a list of scopes which may be used for API access, go to:
      https://developers.google.com/oauthplayground/
    All entered scopes must exactly match an entry shown on this page.

    The returned object may be used by other Google modules as desired.

    If appropriate access has been granted to an alternate email address, the
    following call may be used to override the default email address, using
    one to which read-only access has been granted:

    $client = gauth_svc_acct_client_get(
      'Calendar merge', 
      array('https://www.googleapis.com/auth/calendar.readonly'), 
      someuser@mydomain.com);


API EXAMPLE
-----------

Example code to access Google Calendar where calendar access has been granted:
   
    try {
      $client = gauth_svc_acct_client_get(
        'Calendar merge', 
        array('https://www.googleapis.com/auth/calendar'));
      $service = new Google_Service_Calendar($client);

      Then, use $service in library calls. In this case, all code, including
      variable definition, is contained in the try block. There is no explicit
      disconnection or destruction of variables in a finally block. If such
      cleanup is necessary, $client, $server and necessary other variables
      must be defined outside the try block.

    }
    catch (Exception $e) {
      // The credentials didn't work. Log an error and display as appropriate.
      watchdog('google_calendar_merge_cron Exception',
        $e->getMessage(), $e->getTrace(), WATCHDOG_ERROR);

      // (Keep lines under 80 chars to pass README.txt standards.)
      $msg = 'Saved credentials do not define a service account. ' .
          'See log for details.';

      drupal_set_message(t($msg), 'error');

      Rethrow the exception and/or perform any additional processing. Note
      that cron jobs should rethrow the exception for cron to know that an
      error has occurred.

    }
